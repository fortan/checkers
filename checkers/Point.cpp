#include "Point.h"

Point::Point(float x, float y, float z) : x_(x), y_(y), z_(z)
{
}

float Point::x() 
{ 
	return x_; 
}

float Point::y() 
{
	return y_; 
}

float Point::z() 
{ 
	return z_; 
}