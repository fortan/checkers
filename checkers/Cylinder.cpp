#include "Cylinder.h"
#include "main.h"
extern int generate_id;
Cylinder::Cylinder(const Point& position, const Color& color, const Color& super_color) : position_(position), color_(color), default_color_(color), default_super_color_(super_color), mode_(MODE_JUST)
{
	id_ = ++generate_id;
}

void Cylinder::Draw()
{

	glPushName(id_);
	glColor3f(color_.red(), color_.green(), color_.blue());
	glTranslatef(position_.x(), position_.y(), position_.z());
	GLUquadricObj *pObj = gluNewQuadric();      // �������� ����� quadric-������
	gluQuadricNormals(pObj, GLU_SMOOTH);          // ������� ������������� �����. ��������� � quadric-�������
	gluCylinder(pObj, .45f, .45f, .45f, 26, 100);
	glEnable(GL_CULL_FACE);
	glTranslatef(0, 0, 0.1f);
	//gluDisk(pObj, 0, .43f, 26, 100);
	glTranslatef(0, 0, -0.1f);
	glTranslatef(0, 0, 0.46f);
	gluDisk(pObj, 0, .46f, 26, 100);
	glTranslatef(0, 0, -0.46f);
	glTranslatef(-position_.x(), -position_.y(), -position_.z());
	glDisable(GL_CULL_FACE);
	glPopName();
}

void Cylinder::SetPosition(const Point& position)
{
	position_ = position;
}

void Cylinder::ClearColor()
{
	color_ = mode_ == MODE_JUST ? default_color_ : default_super_color_;
}

void Cylinder::SetColor(const Color& color)
{
	color_ = color;
}

int Cylinder::GetID()
{
	return id_;
}

void Cylinder::SetSuperMode()
{
	mode_ = MODE_SUPER;
	ClearColor();
}

void Cylinder::ClearSuperMode()
{
	mode_ = MODE_JUST;
	ClearColor();
}