#ifndef _CHECKERS_LOGIC_H
#define _CHECKERS_LOGIC_H

#include <map>
#include <vector>
#include <utility>
#define FIGURE_PLAYER1 1
#define SUPER_FIGURE_PLAYER1 2
#define FIGURE_PLAYER2 3
#define SUPER_FIGURE_PLAYER2 4

class CheckersLogic
{
public:
	CheckersLogic();
	std::pair<int, int> GetBoardCoordinate(int id);
	std::pair<int, int> GetFigureCoordinate(int id);
	bool LoosePlayer1();
	bool LoosePlayer2();
	int CheckersLogic::WhoWin();

	struct RobotState
	{
		bool succesful;
		std::vector<int> removes;
		int id;
		std::pair<int, int> to;
	};

	RobotState MakeRobotStep(int player);
	
	int GetFigureMode(int id);
	void PrintBoard();
	std::pair<bool, std::vector<int>> MakeStep(int id, std::pair<int, int> coor);
	void ClearBoard();

	struct Container
	{
		int i;
		int j;
		std::vector<int> removes;
	};

private:
	void RecursiveSuperPlayer1(std::pair<int, int> coor, std::vector<Container>& container);
	void RecursiveSuperPlayer2(std::pair<int, int> coor, std::vector<Container>& container);
	std::vector<Container> GetPossibleSteps(int id);
	void RecursiveSteps(std::pair<int, int> coor, std::vector<int>& removes, std::vector<Container>& container, int type);
	void RecursivePlayer1(std::pair<int, int> coor, std::pair<int, int> prev, std::vector<int>& removes, std::vector<Container>& container, int used[8][8]);
	void RecursivePlayer2(std::pair<int, int> coor, std::pair<int, int> prev, std::vector<int>& removes, std::vector<Container>& container, int used[8][8]);

private:
	std::map<int, std::pair<int, int>> m_figure_mapping;
	std::map<int, std::pair<int, int>> m_board_mapping;
	int m_board[8][8];
};

#endif //_CHECKERS_LOGIC_H