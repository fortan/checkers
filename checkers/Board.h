#ifndef _BOARD_H
#define _BOARD_H

#include "Cube.h"
#include "Cylinder.h"
#include <vector>

class Board
{
public:
	Board();
	void Draw();
	void SetRotates(const std::initializer_list<Rotate>& rotates);
	void HideFigure(int id, int flag = false);
	void MoveFigure(int id, std::pair<int, int> coor);
	void SetFigureColor(int id, const Color& color);
	void ClearFigureColor(int id);
	void ClearBoard();
	void SetFigureSuperMode(int id);
private:
	std::vector<Cube> cubes_;
	std::initializer_list<Rotate> rotates_;
	std::vector<std::pair<bool, Cylinder>> cylinders_;
};

#endif //_BOARD_H