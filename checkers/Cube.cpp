#include "main.h"
#include "Cube.h"

int generate_id = 0;

Cube::Cube(const Point& position, const Color& color, const std::initializer_list<Rotate>& rotates, float scale) : position_(position), color_(color), rotates_(rotates), scale_(scale)
{
	id_ = ++generate_id;
}

void Cube::SetRotates(const std::initializer_list<Rotate>& rotates)
{
	rotates_ = rotates;
}

void Cube::Draw()
{
	glPushName(id_);
	glTranslatef(position_.x(), position_.y(), position_.z());
	glScalef(scale_, scale_, scale_);
	for (const Rotate rotate : rotates_)
		glRotatef(rotate.angle(), rotate.x(), rotate.y(), rotate.z());
	glColor3f(color_.red(), color_.green(), color_.blue());

	// back
	glBegin(GL_POLYGON);
	glNormal3f(0.0f, 0.0f, -0.5f);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();

	// front
	glBegin(GL_POLYGON);
	glNormal3f(0.0f, 0.0f, 0.5f);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glEnd();

	// right
	glBegin(GL_POLYGON);
	glNormal3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glEnd();

	// left
	glBegin(GL_POLYGON);
	glNormal3f(-0.5f, 0.0f, 0.0f);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();

	// top
	glBegin(GL_POLYGON);
	glNormal3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glEnd();

	// bottom
	glBegin(GL_POLYGON);
	glNormal3f(0.0f, -0.5f, 0.0f);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();
	glPopName();

	glTranslatef(-position_.x(), -position_.y(), -position_.z());

}