#ifndef _COLOR_H
#define _COLOR_H

class Color
{
public:
	Color(float red, float green, float blue);
	float red();
	float green();
	float blue();
private:
	float red_;
	float green_;
	float blue_;
};

#endif //_COLOR_H