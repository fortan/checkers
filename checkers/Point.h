#ifndef _POINT_H
#define _POINT_H

class Point
{
public:
	Point(float x, float y, float z);
	float x();
	float y();
	float z();
private:
	float x_;
	float y_;
	float z_;
};
#endif //_POINT_H