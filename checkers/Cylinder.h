#ifndef _CYLINDER_H
#define _CYLINDER_H
#include "Point.h"
#include "Color.h"

#define MODE_JUST 1
#define MODE_SUPER 2

class Cylinder
{
public:
	Cylinder(const Point& position, const Color& color, const Color& super_color);
	void Draw();
	int GetID();
	void SetPosition(const Point& position);
	void ClearColor();
	void SetColor(const Color& color);
	void SetSuperMode();
	void ClearSuperMode();
private:
	Point position_;
	Color color_;
	const Color default_color_;
	const Color default_super_color_;
	int mode_;
	int id_;
};

#endif //_CYLINDER_H