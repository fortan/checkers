#ifndef _MAIN_H
#define _MAIN_H

#include <Windows.h>
#include <cstdio>
#include <cstdlib>
#include <gl/GL.h>
#include <gl/GLU.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define SCREEN_DEPTH 16

extern HWND			g_hWnd;
extern RECT			g_cRect;
extern HDC			g_hDC;
extern HGLRC		g_hRC;
extern HINSTANCE	g_hInstance;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprev, PSTR cmdline, int ishow);

LRESULT CALLBACK WinProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

WPARAM MainLoop();

HWND CreateMyWindows(LPSTR strWindowName, int width, int height, DWORD dwStyle, bool bFullScreen, HINSTANCE hInstance);

bool bSetupPixelFormat(HDC hdc);

void SizeOpenGLScreen(int width, int height);

void InitializeOpenGL(int width, int height);

void Init(HWND hWnd);

void RenderScene();

void DeInit();

#endif //_MAIN_H