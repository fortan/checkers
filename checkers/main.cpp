// �������� ����������� ����������:
// OpelGL
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

// ��� ������� �����:
#include "main.h"
#include "Cube.h"
#include "Board.h"
#include "Cylinder.h"
#include "CheckersLogic.h"
#include <string>
#include <utility>
#include <iostream>
#include <time.h>

void usleep(__int64 usec)
{
	HANDLE timer;
	LARGE_INTEGER ft;

	ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time

	timer = CreateWaitableTimer(NULL, TRUE, NULL);
	SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
	WaitForSingleObject(timer, INFINITE);
	CloseHandle(timer);
}

// ����������� �����������:
HWND  g_hWnd;
RECT  g_rRect;
HDC   g_hDC;
HGLRC g_hRC;
HINSTANCE g_hInstance;

///////////////////////////////////////////////////////////////
//
//          ������� ���������� ������ ���� � �������� �����
//
///////////////////////////////////////////////////////////////
double rtri = 0;
double rtui = 0;
Board board;
void RenderScene()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	board.SetRotates({ Rotate(rtri, 0.0f, 0.1f, 0.0f), Rotate(rtui, 1.0f, 0.0f, 0.0f) });
	board.Draw();
	SwapBuffers(g_hDC);
}

///////////////////////////////////////////////////////////////
//
//          �������������� ������� ����
//
///////////////////////////////////////////////////////////////

void Init(HWND hWnd)
{
	g_hWnd = hWnd;
	GetClientRect(g_hWnd, &g_rRect);
	InitializeOpenGL(g_rRect.right, g_rRect.bottom);
}

///////////////////////////////////////////////////////////////
//
//          ������� ����
//
///////////////////////////////////////////////////////////////

WPARAM MainLoop()
{
	MSG msg;

	while (1)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			RenderScene();
		}
	}

	DeInit();

	return(msg.wParam);
}

///////////////////////////////////////////////////////////////
//
//          ��������� ��������� windows
//
///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//
// ������ ��� ����� �������� �������, ���������� ID ���������� �������:

int RetrieveObjectID(int x, int y)
{
	int objectsFound = 0;   // ����� ���������� ��������� ��������
	int viewportCoords[4] = { 0 };    // ������ ��� �������� �������� ���������


	// ���������� ��� �������� ID ��������, �� ������� �� ��������.
	// �� ������ ������ � 32 ��������, �.�. OpenGL ����� ��������� ������
	// ����������, ������� ��� ������ �� �����. ��� ������� ������� �����
	// 4 �����.
	unsigned int selectBuffer[32] = { 0 };

	// glSelectBuffer ������������ ������ ��� ����� ������ ��������. ������ �������� - ������
	// �������. ������ - ��� ������ ��� �������� ����������.

	glSelectBuffer(32, selectBuffer);   // ������������ ����� ��� �������� ��������� ��������

	// ��� ������� ���������� ���������� � ������ ����� � OpenGL. �� ������� GL_VIEWPOR,
	// ����� �������� ���������� ������. ������� �������� �� � ���������� ������ ���������� �������
	// � ���� top,left,bottom,right.
	glGetIntegerv(GL_VIEWPORT, viewportCoords); // �������� ������� ���������� ������

	// ������ ������� �� ������� GL_MODELVIEW � ��������� � ������� GL_PROJECTION.
	// ��� ��� ����������� ������������ X � Y ���������� ������ 3D.

	glMatrixMode(GL_PROJECTION);    // ��������� � ������� ��������

	glPushMatrix();         // ��������� � ����� �������� ����������

	// ��� ������� ������ ���, ��� ���������� �� ���������� ��� ������� � ����, ������ �����
	// ���������� ������ ��� (ID) ����������, ������� ���� �� ���������� ��� ������
	// GL_RENDER. ���������� ���������� � selectBuffer.

	glRenderMode(GL_SELECT);    // ��������� ��������� ������� ��� ��������� �����������

	glLoadIdentity();       // ������� ������� ��������

	// gluPickMatrix ��������� ��������� ������� �������� ����� ������ �������. ����� ������,
	// ���������� ������ �������, ������� �� ������ (������ �������). ���� ������ ����������
	// � ���� �������, ��� ID ����������� (��� ��, ����� ���� �������).
	// ������ 2 ��������� - X � Y ���������� ������, ��������� 2 - ������ � ������ �������
	// ���������. ��������� �������� - �������� ����������. ��������, �� �������� 'y' ��
	// ������ �������� ����������. �� ������� ���, ����� ����������� Y ����������. 
	// � 3�-������������ ������� y-���������� ���������� �����, � � �������� �����������
	// 0 �� y ��������� ������. ����� ������� ������ 2 �� 2 ������� ��� ������ � ��� �������. 
	// ��� ����� ���� �������� ��� ��� �������.

	gluPickMatrix(x, viewportCoords[3] - y, 2, 2, viewportCoords);

	// ����� ������ �������� ���� ���������� ������� gluPerspective, ����� ��� ��, ���
	// ������ ��� �������������.

	gluPerspective(45.0f, (float)g_rRect.right / (float)g_rRect.bottom, 0.1f, 150.0f);

	glMatrixMode(GL_MODELVIEW); // ������������ � ������� GL_MODELVIEW

	RenderScene();          // ������ �������� ��������� ���� ��� ������ �������

	// ���� �� �������� � ���������� ����� ���������� �� ������ ������, glRenderMode
	// ��������� ����� ��������, ��������� � ��������� ������� (� gluPickMatrix()).

	objectsFound = glRenderMode(GL_RENDER); // �������� � ����� ��������� � ������� ����� ��������

	glMatrixMode(GL_PROJECTION);    // �������� � ��������� ������� ��������
	glPopMatrix();              // ������� �� �������

	glMatrixMode(GL_MODELVIEW);     // �������� � ������� GL_MODELVIEW

	// ���! ��� ���� ������� ������. ������ ��� ����� �������� ID ��������� ��������.
	// ���� ��� ���� - objectsFound ������ ���� ��� ������� 1.

	if (objectsFound > 0)
	{
		// ���� �� ����� ����� 1 �������, ����� ��������� �������� ������� ����
		// ��������� ��������. ������ � ������� ��������� ������� - ���������
		// � ��� ������, ������ � �������� �� �� ����. � ����������� �� ����, ���
		// �� �������������, ��� ����� ����������� � ��� ��������� ������� (����
		// ��������� ���� �� ���������), �� � ���� ����� �� ����������� ������ �
		// �������� �������. ����, ��� ��� �������� �������� �������? ��� ���������
		// � ������ ������ (selectionBuffer). ��� ������� ������� � ��� 4 ��������.
		// ������ - "����� ���� � ������� ���� �� ������ �������, ����� ������� �
		// �������� �������� ������� ��� ���� ������, ������� ���� ������� ��� �������
		// �������, ����� �� ���������� ������� ����, ������ ��� - ������;
		// ("the number of names in the name stack at the time of the event, followed
		// by the minimum and maximum depth values of all vertices that hit since the
		// previous event, then followed by the name stack contents, bottom name first.") - MSDN.
		// ��������, ��� ��� ����� - ����������� �������� ������� (������ ��������) �
		// ID �������, ����������� � glLoadName() (��������� ��������).
		// ����, [0-3] - ������ ������� �������, [4-7] - �������, � �.�...
		// ����� ���������, ��� ��� ���� �� ����������� �� ������ 2� �����, �� ����� 
		// ������ ��������� ��� ��������� ������. ��� ��� ���������, ��� ��������� �����
		// ������ ��� ���������� � ������ GL_SELECT. � ��� ����� ��������� ����, ������������
		// � RenderScene(). ����, ������� ������ � ����������� ��������!

		// ��� ������ ��������� ��������� ������� ��� ������� ������� �������.
		// 1 - ��� ����������� Z-�������� ������� �������.
		unsigned int lowestDepth = selectBuffer[1];

		// ��������� ��������� ������ ��� ������ ��� ������.
		// 3 - ID ������� �������, ���������� � glLoadName().
		int selectedObject = selectBuffer[3];

		// �������� ����� ��� ��������� �������, ������� �� ������� (�������� �������
		// �� ��������� ����������).
		for (int i = 1; i < objectsFound; i++)
		{
			// ���������, �� ���� �� �������� ������� �������� �������, ��� �����������.
			// ��������, �� �������� i �� 4 (4 �������� �� ������ ������) � ���������� 1 ��� �������.
			if (selectBuffer[(i * 4) + 1] < lowestDepth)
			{
				// ��������� ����� ������ ��������
				lowestDepth = selectBuffer[(i * 4) + 1];

				// ��������� ������� ID �������
				selectedObject = selectBuffer[(i * 4) + 3];
			}
		}

		// ������ ��������� ������
		return selectedObject;
	}

	// ���� �� �������� �� �� 1 ������, ������ 0
	return 0;
}

int srcObj = -1;
CheckersLogic logic;
int playerState = 1;

void NewGame()
{
	board.ClearBoard();
	logic.ClearBoard();
	srcObj = -1;
	playerState = 1;
}
LRESULT CALLBACK WinProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LONG    lRet = 0;
	PAINTSTRUCT    ps;
	std::string s;
	if (playerState == 3)
	{
		if (logic.WhoWin() == 0)
		{
			usleep(1000);
			CheckersLogic::RobotState el = logic.MakeRobotStep(playerState);
			if (el.succesful)
			{
				if (logic.GetFigureMode(el.id) == SUPER_FIGURE_PLAYER2 || logic.GetFigureMode(el.id) == SUPER_FIGURE_PLAYER1)
					board.SetFigureSuperMode(el.id);
				for (auto & t : el.removes)
					board.HideFigure(t);
				board.MoveFigure(el.id, el.to);
				board.ClearFigureColor(el.succesful);
				playerState = 1;
			}
		}
		if (logic.WhoWin() != 0)
		{
			playerState = 10;
			s = "������� �����: " + std::to_string(logic.WhoWin());
			MessageBox(hWnd, s.c_str(), "Click", MB_OK);
			NewGame();
		}

	}
	switch (uMsg)
	{
	case WM_LBUTTONDOWN: {
			
			// ������� ���������� ������� � �������, ������������ ������

			std::cout << "Player State " << playerState << std::endl;
			if (playerState == 1)
			{
				if (logic.WhoWin() == 0)
				{
					int objectID;
					objectID = RetrieveObjectID(LOWORD(lParam), HIWORD(lParam));
					std::pair<int, int> figure = logic.GetFigureCoordinate(objectID);
					std::pair<int, int> cube = logic.GetBoardCoordinate(objectID);

					if (figure.first != -1)
					{
						board.ClearFigureColor(srcObj);
						srcObj = objectID;
						board.SetFigureColor(srcObj, Color(0.800, 0.1, 0.1));
					}

					if (objectID == 0)
					{
						board.ClearFigureColor(srcObj);


						srcObj = -1;
					}

					if (srcObj != -1 && cube.first != -1)
					{
						std::pair<bool, std::vector<int>> el = logic.MakeStep(srcObj, cube);
						if (el.first)
						{
							if (logic.GetFigureMode(srcObj) == SUPER_FIGURE_PLAYER2 || logic.GetFigureMode(srcObj) == SUPER_FIGURE_PLAYER1)
								board.SetFigureSuperMode(srcObj);
							for (auto & t : el.second)
								board.HideFigure(t);
							board.MoveFigure(srcObj, cube);
							board.ClearFigureColor(srcObj);
							srcObj = -1;
							playerState = 3;
						}
					}
				}
				if (logic.WhoWin() != 0)
				{
					playerState = 10;
					s = "������� �����: " + std::to_string(logic.WhoWin());
					MessageBox(hWnd, s.c_str(), "Click", MB_OK);
					NewGame();
				}
			}

			

		}
		break;
	case WM_SIZE:       // ���� ������� ������ ����

		SizeOpenGLScreen(LOWORD(lParam), HIWORD(lParam));// LoWord=Width, HiWord=Height
		GetClientRect(hWnd, &g_rRect);      // �������� window rectangle
		break;

	case WM_PAINT:          // ���� ����� ������������ �����
		BeginPaint(hWnd, &ps);  // ����. paint struct
		EndPaint(hWnd, &ps);    // EndPaint, ���������
		break;

	case WM_KEYDOWN:    // ��� ��������� ��������, ��� ������ ������� �� ����������.
		// ���� ������� ��������� � ��������� wParam
		switch (wParam)
		{
		case VK_ESCAPE:         // ���� ����� ESCAPE
			PostQuitMessage(0); // �������
			break;
		case VK_RIGHT: rtri += 3.0; break;
		case VK_LEFT: rtri -= 3.0; break;
		case VK_UP: rtui += 3.0; break;
		case VK_DOWN: rtui -= 3.0; break;
		}
		break;
	case WM_CLOSE:      // ���� ���� ���� �������
		PostQuitMessage(0); // �������
		break;

	default:        // Return �� ���������
		lRet = DefWindowProc(hWnd, uMsg, wParam, lParam);
		break;
	}

	return lRet;
}