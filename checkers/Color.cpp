#include "Color.h"

Color::Color(float red, float green, float blue) : red_(red), green_(green), blue_(blue)
{
}

float Color::red() 
{ 
	return red_; 
}

float Color::green() 
{ 
	return green_; 
}

float Color::blue() 
{ 
	return blue_; 
}