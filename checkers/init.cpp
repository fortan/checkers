// �������� ��� �����
#include "main.h"

///////////////////////////////////////////////////////////////
//
//      �������, ��������� ���� ������
//
///////////////////////////////////////////////////////////////
HWND CreateMyWindow(LPSTR strWindowName, int width, int height, DWORD dwStyle, bool bFullScreen, HINSTANCE hInstance)
{
	HWND hWnd;      // ���������� ����
	WNDCLASS wndclass;  // ����� ����
	memset(&wndclass, 0, sizeof(WNDCLASS));     // ����������� ������ ��� ����� ����
	wndclass.style = CS_HREDRAW | CS_VREDRAW;   // ����������� ���������
	wndclass.lpfnWndProc = WinProc;         // ������� ��������� �� ������� ��������� ���������
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);   // ������
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);     // ������
	wndclass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); // ���� ����� �����
	wndclass.lpszClassName = "Wingman`s 3dLessons";     // ��� ������
	RegisterClass(&wndclass);               //������������ �����

	dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN; // ����� ����

	g_hInstance = hInstance;

	// ������������� �������� ������� ����.
	RECT rWindow;
	rWindow.left = 0;            // ����� ������� -  0
	rWindow.right = width;        // ������ ������� - 800
	rWindow.top = 0;        // ���� - 0
	rWindow.bottom = height;       // ��� - 800

	AdjustWindowRect(&rWindow, dwStyle, false);    // ��������� �������� �������

	// ������ ����
	hWnd = CreateWindow("Wingman`s 3dLessons", strWindowName, dwStyle, 0, 0,
		rWindow.right - rWindow.left, rWindow.bottom - rWindow.top,
		NULL, NULL, hInstance, NULL);

	if (!hWnd) return NULL;          // ���� �� ���������� - �������...
	ShowWindow(hWnd, SW_SHOWNORMAL);    // �������� ����
	UpdateWindow(hWnd);         // � ���������� ���
	SetFocus(hWnd);             // ���������� ���������� �� ���� ����

	return hWnd;
}


///////////////////////////////////////////////////////////////
//
//      �������, ��������������� ������ �������
//
///////////////////////////////////////////////////////////////
bool bSetupPixelFormat(HDC hdc)
{
	PIXELFORMATDESCRIPTOR pfd;  // ���������� ������� �������
	int pixelformat;
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);  // ������������� ������ ���������
	pfd.nVersion = 1;               // ������ ������ = 1
	// ������� ������ ����� OpenGL
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.dwLayerMask = PFD_MAIN_PLANE;       // ����������� ����� (���� ���� ������������)
	pfd.iPixelType = PFD_TYPE_RGBA;     // ��� ����� RGB and Alpha ���� ��������
	pfd.cColorBits = SCREEN_DEPTH;      // ���������� ���� #define ��� �������� �������
	pfd.cDepthBits = SCREEN_DEPTH;  // ��� ������������ ��� RGBA, �� ��� ����� ���������
	pfd.cAccumBits = 0;
	pfd.cStencilBits = 0;

	// �-� ���� ������ �������, �������� ���������� �������� �����������, ����� ��� �������
	if ((pixelformat = ChoosePixelFormat(hdc, &pfd)) == FALSE)
	{
		MessageBox(NULL, "ChoosePixelFormat failed", "Error", MB_OK);
		return FALSE;
	}

	// ������������� ��������� ������ �������
	if (SetPixelFormat(hdc, pixelformat, &pfd) == FALSE)
	{
		MessageBox(NULL, "SetPixelFormat failed", "Error", MB_OK);
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////
//
//      �-� ������������� ������ ���� OpenGL
//
///////////////////////////////////////////////////////////////
void SizeOpenGLScreen(int width, int height)
{
	if (height == 0)      // ������������ ������� �� 0
		height = 1;

	// ������� ���� ���� OpenGL �������� � ������� ���� ���������. ��� ������� ����� �������
	// ������� ������ ����.
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);    // ������� ������� ��������
	glLoadIdentity();       // � ������� �

	// �������� ����������� ������ ����
	// ���������:
	// (���� �������, ��������� ������ � ������,
	// ��������� ���������� ������� �� ������, ��� ������� �� �����,
	// � ���������� ����������, ��� �������� ���������� ���������
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, .5f, 150.0f);

	// �����: ������� ��������� ������ ���� ������ �������, ���� �� �� ������ ��������
	// ���������� ���������, ������� � ����������. ���� ��� ����� �� ����, �� ���
	// ������������� ����� � ������� ���������� ������ ������ 1, �� ��������� �����
	// ����� ���������� ����� � �����.

	glMatrixMode(GL_MODELVIEW);  // ������� ������� �������
	glLoadIdentity();            // � ������� �
}


///////////////////////////////////////////////////////////////
//
//          �-� �������������� OpenGL
//
///////////////////////////////////////////////////////////////
float g_LightPosition[4] = { 0, 1, 0, 1 };         // ������� ��������� �����
float g_bLight = true;                           // ������� �� ����
void InitializeOpenGL(int width, int height)
{
	g_hDC = GetDC(g_hWnd);  // ������������� ���������� ���������� ����

	if (!bSetupPixelFormat(g_hDC))      // ������������� ������ �������
		PostQuitMessage(0);            // � ������� ��� ������

	g_hRC = wglCreateContext(g_hDC);        // �������� ���������� ��� hdc
	wglMakeCurrent(g_hDC, g_hRC);       // ������ �������� �������
	//glEnable(GL_TEXTURE_2D);            // �������� ��������
	glEnable(GL_DEPTH_TEST);            // � ���� �������

	glEnable(GL_DEPTH_TEST);

	// � ������������� ������ ��������:
	SizeOpenGLScreen(width, height);

	glClearColor(0.467, 0.237, 0.893, 1);

	// ���� �������� ��� ������� ������. ������, ambience[] - ���� "����������� �����", �����
	// �� ������� �� ������ ������ ���������. ���� �� �� ��������� ���� ����, �� ���������� �������� 
	// ����� ���������� �������, � �� �� �� ������. ���� ��������� �� ������� �����, � ����������
	// ���� ��������� � �������� ����� ��������.
	// ������ ������, diffuse[], ��� ���� ������������� �����. �� ������� �����. ������ ���
	// ����� ������� - R,G,B, ��������� - �������� �����. ���� ��� �� ������������ � ���.
	//
	// ��� ����������� ����� ������� ������� ����� ����, ��� ��������� - ������� �����
	// ����� � ������.

	float ambience[4] = { 0.5f, 0.5f, 0.5f, 1.0 };    // ���� �������� �����
	float diffuse[4] = { 0.5f, 0.5f, 0.5f, 1.0 }; // ���� ������������ �����

	// ����� ���������� ������� ��������, ����� �������� OpenGL ���� �������.
	// OpenGL ��� ��� ����������� ������������ ��������� ���������� �����. ����� ����������
	// ���������� ������� �� ���������� GL_MAX_LIGHTS. ��� ������� ������������� ��������� �����
	// ������������ ������ OpenGL GL_LIGHT0. ����� �������� ������������� ��������� ������� 
	// OpenGL ���� ���������, ��� �� ������������� �������� ambience, � ������ ambience-������.
	// ����� �� �� ������ � diffuse � GL_DIFFUSE.

	// ������������� ���� ����������� ����� (��� ������������� �����)
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambience);
	// � ��������� ���� (���� �����)
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);

	// ����� ��� ����� ���������� ������� ��������� �����. � ��� ����� ���� ����� �������
	// ���������� � ������ ������, �� � ���� ����� ����� �������������� ������ ����.
	// ��� ����������� ���������� ����� ������������ GL_LIGHT1, GL_LIGHT2, GL_LIGHT3 � �.�...
	// �� ������ �������� ������� ��������� ��������� '+' � '-'. ������� �� ��������� - (0,1,0,1).
	// ��� ���������� �������� ����� ��� ����������� ���������. ��������� �������� � �������
	// g_LightPosition �������� OpenGL, ����� �� ��� ����������� ��� ������������ ����.
	// ���� �������� = 1, ���� ����� �����������, ����� �� ����� ��������� �� ������.
	// ���� �� ��������� ������������ ����, �� ����� �������� �� "�� ������" �� ������ "�����".
	// ���� �� ���� ����� �����������, �� "��������" � ����������� x,y,z � ����� �������� 
	// �� ������ ����. ��� ��, ��� ��� �����, ��� ��� ��������� �������� � �������.

	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, g_LightPosition);

	// ����� ������������� ��������� ����� ��� ����� �������� ���:
	glEnable(GL_LIGHT0);

	// �� ������������ �������� ���� ��������; ����� ����� ����� �������� ���� 
	// ��������� � OpenGL:
	glEnable(GL_LIGHTING);

	// ��������� ������ ��������� ����������� �������� ������ ��� ���������� ���������:
	glEnable(GL_COLOR_MATERIAL);

	// ����������� �����
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	// ����������� �����
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	// ����������� ���������    
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

}

///////////////////////////////////////////////////////////////
//
//          �-� ��-�������������� OpenGL
//
///////////////////////////////////////////////////////////////
void DeInit()
{
	if (g_hRC)
	{
		wglMakeCurrent(NULL, NULL); // ����������� ������, ������� ��� �������
		wglDeleteContext(g_hRC);    // ������� �������� ���������� OpenGL
	}

	if (g_hDC)
		ReleaseDC(g_hWnd, g_hDC);   // ������� HDC �� ������

	UnregisterClass("Wingman`s 3dLessons", g_hInstance);    // ����������� ����� ����
	PostQuitMessage(0);                    // �������
}

///////////////////////////////////////////////////////////////
//
//          ������� ������������ � ������� ����
//
///////////////////////////////////////////////////////////////
#include <windows.h> 
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
BOOL CreateConsole(void)
{
	FreeConsole(); //�� ������ ������
	if (AllocConsole())
	{
		int hCrt = _open_osfhandle((long)
			GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
		*stdout = *(::_fdopen(hCrt, "w"));
		::setvbuf(stdout, NULL, _IONBF, 0);
		*stderr = *(::_fdopen(hCrt, "w"));
		::setvbuf(stderr, NULL, _IONBF, 0);
		return TRUE;
	}
	return FALSE;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hprev, PSTR cmdline, int ishow)
{
	CreateConsole();
	HWND hWnd;

	// ������� ���� � ������� ����� �������, � ������� �������:
	// ���, ������, ������, ����� ����� ��� ����, ����� �� �� ��������, � hInstance
	hWnd = CreateMyWindow("Wingman`s 3dLessons", SCREEN_WIDTH, SCREEN_HEIGHT, 0, false, hInstance);

	// ������� ��� ������
	if (hWnd == NULL) return TRUE;

	// �������������� OpenGL
	Init(hWnd);

	// ��������� ������� ����
	return MainLoop();
}