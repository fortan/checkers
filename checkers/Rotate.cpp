#include "Rotate.h"

Rotate::Rotate(float angle, float x, float y, float z) : x_(x), y_(y), z_(z), angle_(angle)
{
}

float Rotate::x() const
{
	return x_;
}

float Rotate::y() const
{
	return y_;
}

float Rotate::z() const
{
	return z_;
}

float Rotate::angle() const
{
	return angle_;
}