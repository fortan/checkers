#ifndef _CUBE_H
#define _CUBE_H
#include "Point.h"
#include "Color.h"
#include "Rotate.h"
#include <initializer_list>

class Cube
{
public:
	Cube(const Point& position, const Color& color, const std::initializer_list<Rotate>& rotates = {}, float scale = 1.0);
	void Draw();
	void SetRotates(const std::initializer_list<Rotate>& rotates);
private:
	Point position_;
	Color color_;
	std::initializer_list<Rotate> rotates_;
	float scale_;
	int id_;
};

#endif //_CUBE_H