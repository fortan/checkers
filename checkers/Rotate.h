#ifndef _ROTATE_H
#define _ROTATE_H

class Rotate
{
public:
	Rotate(float angle, float x, float y, float z);
	float x() const;
	float y() const;
	float z() const;
	float angle() const;
private:
	float x_;
	float y_;
	float z_;
	float angle_;
};
#endif //_ROTATE_H