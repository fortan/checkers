#include "Board.h"
#include <utility>

Board::Board()
{
	cubes_.push_back(Cube(Point(0, 0, -5.f), Color(0, 0, 0), {}, 0.4));
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (3 - i != 0 || 3 - j != 0) cubes_.push_back(Cube(Point(3 - j, 3 - i, -5.f), Color((i + j) % 2, (i + j) % 2, (i + j) % 2), {}));

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 8; j++)
			if ((i + j) % 2 == 0) cylinders_.push_back(std::make_pair(true, Cylinder(Point(3 - j, 3 - i, -4.5), Color(0.285f, 0.176f, 0.13f), Color(0.4f, 0.3f, 0.3f))));

	for (int i = 5; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if ((i + j) % 2 == 0) cylinders_.push_back(std::make_pair(true, Cylinder(Point(3 - j, 3 - i, -4.5), Color(0.485f, 0.476f, 0.43f), Color(0.6f, 0.6f, 0.58f))));
}

void Board::Draw()
{
	if (!cubes_.empty())
		cubes_[0].SetRotates(rotates_);
	for (auto & cube : cubes_)
		cube.Draw();
	for (auto & cylinder : cylinders_)
		if (cylinder.first) cylinder.second.Draw();
}

void Board::HideFigure(int id, int flag)
{
	for (auto & cylinder : cylinders_)
		if (cylinder.second.GetID() == id)
		{
			cylinder.first = flag;
			break;
		}
}

void Board::MoveFigure(int id, std::pair<int, int> coor)
{
	int i = 3 - coor.first, j = -4 + coor.second;
	for (auto & cylinder : cylinders_)
		if (cylinder.second.GetID() == id)
		{
			cylinder.second.SetPosition(Point(j, i, -4.5f));
			break;
		}
}

void Board::SetFigureColor(int id, const Color& color)
{
	for (auto & cylinder : cylinders_)
		if (cylinder.second.GetID() == id)
		{
			cylinder.second.SetColor(color);
			break;
		}
}

void Board::ClearFigureColor(int id)
{
	for (auto & cylinder : cylinders_)
		if (cylinder.second.GetID() == id)
		{
			cylinder.second.ClearColor();
			break;
		}
}

void Board::SetFigureSuperMode(int id)
{
	for (auto & cylinder : cylinders_)
		if (cylinder.second.GetID() == id)
		{
			cylinder.second.SetSuperMode();
			break;
		}
}

void Board::SetRotates(const std::initializer_list<Rotate>& rotates) 
{
	rotates_ = rotates;
}

void Board::ClearBoard()
{
	int id = 65;

	for (int i = 0; i < 3; i++)
		for (int j = 7; j >= 0; j--)
			if ((i + j) % 2 == 1) { MoveFigure(id, std::make_pair(i, j)); id++; }
	
	for (int i = 5; i < 8; i++)
		for (int j = 7; j >= 0; j--)
			if ((i + j) % 2 == 1)  { MoveFigure(id, std::make_pair(i, j)); id++; }
	
	for (auto & cylinder : cylinders_)
	{
		cylinder.first = true;
		cylinder.second.ClearColor();
		cylinder.second.ClearSuperMode();
	}
}