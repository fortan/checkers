#include "CheckersLogic.h"
#include <map>
#include <iostream>
#include <algorithm>
#include <ctime>

CheckersLogic::CheckersLogic()
{
	int id = 1;
	m_board_mapping.insert(std::make_pair(id, std::make_pair(3, 4)));
	id++;
	for (int i = 0; i < 8; i++)
		for (int j = 7; j >= 0; j--)
			if (i != 3 || j != 4) { m_board_mapping.insert(std::make_pair(id, std::make_pair(i, j))); id++; }

	for (int i = 0; i < 3; i++)
		for (int j = 7; j >= 0; j--)
			if ((i + j) % 2 == 1) { m_figure_mapping.insert(std::make_pair(id, std::make_pair(i, j))); id++; }

	for (int i = 5; i < 8; i++)
		for (int j = 7; j >= 0; j--)
			if ((i + j) % 2 == 1)  { m_figure_mapping.insert(std::make_pair(id, std::make_pair(i, j))); id++; }
	
	ClearBoard();
}

std::pair<int, int> CheckersLogic::GetBoardCoordinate(int id)
{
	auto & itr = m_board_mapping.find(id);
	return itr != m_board_mapping.end() ? itr->second : std::make_pair(-1, -1);
}

void CheckersLogic::ClearBoard()
{
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			m_board[i][j] = 0;

	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 8; j++)
			if ((i + j) % 2 == 1) m_board[i][j] = FIGURE_PLAYER2;

	for (int i = 5; i < 8; i++)
		for (int j = 0; j <= 8; j++)
			if ((i + j) % 2 == 1)  m_board[i][j] = FIGURE_PLAYER1;
	m_figure_mapping.clear();
	int id = 65;
	for (int i = 0; i < 3; i++)
		for (int j = 7; j >= 0; j--)
			if ((i + j) % 2 == 1) { m_figure_mapping.insert(std::make_pair(id, std::make_pair(i, j))); id++; }

	for (int i = 5; i < 8; i++)
		for (int j = 7; j >= 0; j--)
			if ((i + j) % 2 == 1)  { m_figure_mapping.insert(std::make_pair(id, std::make_pair(i, j))); id++; }
}

std::pair<int, int> CheckersLogic::GetFigureCoordinate(int id)
{
	auto & itr = m_figure_mapping.find(id);
	return itr != m_figure_mapping.end() ? itr->second : std::make_pair(-1, -1);
}

int CheckersLogic::WhoWin()
{
	if (LoosePlayer1()) return 2;
	if (LoosePlayer2()) return 1;
	return 0;
}

bool CheckersLogic::LoosePlayer1()
{
	std::vector < std::pair<int, CheckersLogic::Container>> pool;
	for (auto & v : m_figure_mapping)
	{
		if (GetFigureMode(v.first) == FIGURE_PLAYER1 || GetFigureMode(v.first) == SUPER_FIGURE_PLAYER1)
		{
			std::vector<CheckersLogic::Container> steps = GetPossibleSteps(v.first);
			for (auto & t : steps)
				pool.push_back(std::make_pair(v.first, t));
		}
	}
	return pool.size() == 0;
}

bool CheckersLogic::LoosePlayer2()
{
	std::vector < std::pair<int, CheckersLogic::Container>> pool;
	for (auto & v : m_figure_mapping)
	{
		if (GetFigureMode(v.first) == FIGURE_PLAYER2 || GetFigureMode(v.first) == SUPER_FIGURE_PLAYER2)
		{
			std::vector<CheckersLogic::Container> steps = GetPossibleSteps(v.first);
			for (auto & t : steps)
				pool.push_back(std::make_pair(v.first, t));
		}
	}
	return pool.size() == 0;
}

std::vector<CheckersLogic::Container> CheckersLogic::GetPossibleSteps(int id)
{
	std::vector<Container> list_containers;
	std::vector<Container> split_containers;
	std::pair<int, int> coor = GetFigureCoordinate(id);
	
	if (coor.first != -1)
	{
		std::vector<int> removes;
		RecursiveSteps(coor, removes, list_containers, m_board[coor.first][coor.second]);	
	}

	for (auto & v : list_containers)
	{
		v.removes.erase(std::unique(v.removes.begin(), v.removes.end()), v.removes.end());
		if (v.removes.size() == 1 && GetFigureCoordinate(v.removes[0]) == GetFigureCoordinate(id))
			v.removes.clear();
		if (!(v.removes.size() < 4 && GetFigureCoordinate(id) == std::make_pair(v.i, v.j)))
		{
			split_containers.push_back(v);
		}
	}

	return split_containers;
}

void CheckersLogic::PrintBoard()
{
	std::cout << "Board:" << std::endl;

	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
			std::cout << m_board[i][j] << " ";
		std::cout << std::endl;
	}
}

CheckersLogic::RobotState CheckersLogic::MakeRobotStep(int player)
{
	std::vector < std::pair<int, CheckersLogic::Container>> pool;
	for (auto & v : m_figure_mapping)
	{
		if (GetFigureMode(v.first) == player || GetFigureMode(v.first) == player + 1)
		{
			std::vector<CheckersLogic::Container> steps = GetPossibleSteps(v.first);
			for (auto & t : steps)
				pool.push_back(std::make_pair(v.first, t));
		}
	}

	int max = 0;
	for (auto & v : pool)
		max = std::max(max, (int)v.second.removes.size());

	std::vector < std::pair<int, CheckersLogic::Container>> relevants;
	for (auto & v : pool)
		if (max == (int)v.second.removes.size()) relevants.push_back(v);

	for (auto & v : relevants)
	{
		std::cout << "RELEVANT = " << GetFigureCoordinate(v.first).first << " " << GetFigureCoordinate(v.first).second << " " << v.second.i << " " << v.second.j << std::endl;
		for (int i = 0; i < v.second.removes.size(); i++)
			std::cout << v.second.removes[i] << " ";
		std::cout << std::endl;
	}
	if (relevants.size() > 0)
	{
		srand(time(NULL));
		int i = rand() % relevants.size();

		std::pair<bool, std::vector<int>> tmp = MakeStep(relevants[i].first, std::make_pair(relevants[i].second.i, relevants[i].second.j));
		RobotState state;
		state.succesful = tmp.first;
		state.removes = tmp.second;
		state.id = relevants[i].first;
		state.to = std::make_pair(relevants[i].second.i, relevants[i].second.j);
		std::cout << "ROBOT = [" << GetFigureCoordinate(relevants[i].first).first << " " << GetFigureCoordinate(relevants[i].first).second << "] to [" <<
			relevants[i].second.i << " " << relevants[i].second.j << "] " << std::endl;
		return state;
	}
	RobotState state;
	state.succesful = false;
	return state;
	
}

std::pair<bool, std::vector<int>> CheckersLogic::MakeStep(int id, std::pair<int, int> coor)
{
	std::pair<bool, std::vector<int>> state;
	std::vector<CheckersLogic::Container> steps = GetPossibleSteps(id);
	

	std::cout << "Size Possible Steps = " << steps.size() << " [ " << std::endl;
	for (auto & v : steps)
	{
		std::cout << "i = " << v.i << " j = " << v.j << " size = " << v.removes.size() << " (";
		for (int z = 0; z < v.removes.size(); z++)
			std::cout << "[ " << GetFigureCoordinate(v.removes[z]).first << "," << GetFigureCoordinate(v.removes[z]).second << "] ";
		std::cout << ")" << std::endl;
	}
	std::cout << std::endl << " ] " << std::endl;
	for (auto & v : steps)
	{
		if (std::make_pair(v.i, v.j) == coor)
		{
			int count = 0;
			for (auto t : v.removes)
			{
				auto f = GetFigureCoordinate(id);
				if (m_board[f.first][f.second] == FIGURE_PLAYER2 || m_board[f.first][f.second] == SUPER_FIGURE_PLAYER2)
				{
					if (GetFigureCoordinate(t).first == 6) count++;
				}
				else
					if (GetFigureCoordinate(t).first == 1) count++;
			}

			if (count > 1)
			{
				auto f = GetFigureCoordinate(id);
				if (m_board[f.first][f.second] == FIGURE_PLAYER2 || m_board[f.first][f.second] == SUPER_FIGURE_PLAYER2)
				{
					m_board[f.first][f.second] = SUPER_FIGURE_PLAYER2;
				}
				else
					m_board[f.first][f.second] = SUPER_FIGURE_PLAYER1;
			}
			{
				auto f = GetFigureCoordinate(id);
				if ((m_board[f.first][f.second] == FIGURE_PLAYER2 || m_board[f.first][f.second] == SUPER_FIGURE_PLAYER2) && coor.first == 7)
				{
					m_board[f.first][f.second] = SUPER_FIGURE_PLAYER2;
				}

				if ((m_board[f.first][f.second] == FIGURE_PLAYER1 || m_board[f.first][f.second] == SUPER_FIGURE_PLAYER1) && coor.first == 0)
				{
					m_board[f.first][f.second] = SUPER_FIGURE_PLAYER1;
				}
			}

			std::cout << "COUNT = " << count << std::endl;
					
			state.first = true;
			state.second = v.removes;
			for (auto t : state.second)
			{
				auto f = GetFigureCoordinate(t);
				m_board[f.first][f.second] = 0;
				m_figure_mapping.erase(t);
			}
			auto f = GetFigureCoordinate(id);
			m_figure_mapping.erase(id);
			m_board[coor.first][coor.second] = m_board[f.first][f.second];
			if (coor != f) m_board[f.first][f.second] = 0;
			m_figure_mapping.insert(std::make_pair(id, coor));
			PrintBoard();
			return state;
		}
	}
	PrintBoard();
	return state;
}

int CheckersLogic::GetFigureMode(int id)
{
	std::pair<int, int> p = GetFigureCoordinate(id);
	return m_board[p.first][p.second];
}

void CheckersLogic::RecursiveSteps(std::pair<int, int> coor, std::vector<int>& removes, std::vector<Container>& container, int type)
{
	int used[8][8];
	memset(used, 0, sizeof(used));

	switch (type)
	{
	case FIGURE_PLAYER1:
		RecursivePlayer1(coor, std::pair<int, int>(-1, -1), removes, container, used);
		break;
	case FIGURE_PLAYER2:
		RecursivePlayer2(coor, std::pair<int, int>(-1, -1), removes, container, used);
		break;
		
	case SUPER_FIGURE_PLAYER1:
		RecursiveSuperPlayer1(coor, container);
		break;
	case SUPER_FIGURE_PLAYER2:
		RecursiveSuperPlayer2(coor, container);
		break;
	}
}

void CheckersLogic::RecursiveSuperPlayer1(std::pair<int, int> coor, std::vector<Container>& container)
{
	int i, j;
	std::vector<int> removes;
	i = coor.first + 1; j = coor.second + 1;
	while (i < 8 && j < 8)
	{
		if (m_board[i][j] == 0) container.push_back(Container({ i, j, removes }));
		else
		if (m_board[i][j] == FIGURE_PLAYER1 || m_board[i][j] == SUPER_FIGURE_PLAYER1) break;
		else
		if (i + 1 < 8 && j + 1 < 8 && (m_board[i][j] == FIGURE_PLAYER2 || m_board[i][j] == SUPER_FIGURE_PLAYER2) && m_board[i+1][j+1] == 0)
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j))
				{
					removes.push_back(v.first);
					break;
				}
		}
		else break;
		i++; j++;
	}
	removes.clear();

	i = coor.first - 1; j = coor.second - 1;
	while (i >= 0 && j >= 0)
	{
		if (m_board[i][j] == 0) container.push_back(Container({ i, j, removes }));
		else
		if (m_board[i][j] == FIGURE_PLAYER1 || m_board[i][j] == SUPER_FIGURE_PLAYER1) break;
		else
		if (i - 1 >= 0 && j - 1 >= 0 && (m_board[i][j] == FIGURE_PLAYER2 || m_board[i][j] == SUPER_FIGURE_PLAYER2) && m_board[i - 1][j - 1] == 0)
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j))
				{
					removes.push_back(v.first);
					break;
				}
		}
		else break;
		i--; j--;
	}
	removes.clear();

	i = coor.first - 1; j = coor.second + 1;
	while (i >= 0 && j < 8)
	{
		if (m_board[i][j] == 0) container.push_back(Container({ i, j, removes }));
		else
		if (m_board[i][j] == FIGURE_PLAYER1 || m_board[i][j] == SUPER_FIGURE_PLAYER1) break;
		else
		if (i - 1 >= 0 && j + 1 < 8 && (m_board[i][j] == FIGURE_PLAYER2 || m_board[i][j] == SUPER_FIGURE_PLAYER2) && m_board[i - 1][j + 1] == 0)
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j))
				{
					removes.push_back(v.first);
					break;
				}
		}
		else break;
		i--; j++;
	}
	removes.clear();

	i = coor.first + 1; j = coor.second - 1;
	while (i < 8 && j >= 0)
	{
		if (m_board[i][j] == 0) container.push_back(Container({ i, j, removes }));
		else
		if (m_board[i][j] == FIGURE_PLAYER1 || m_board[i][j] == SUPER_FIGURE_PLAYER1) break;
		else
		if (i + 1 < 8 && j - 1 >= 0 && (m_board[i][j] == FIGURE_PLAYER2 || m_board[i][j] == SUPER_FIGURE_PLAYER2) && m_board[i + 1][j - 1] == 0)
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j))
				{
					removes.push_back(v.first);
					break;
				}
		}
		else break;
		i++; j--;
	}
	removes.clear();
}

void CheckersLogic::RecursiveSuperPlayer2(std::pair<int, int> coor, std::vector<Container>& container)
{
	int i, j;
	std::vector<int> removes;
	i = coor.first + 1; j = coor.second + 1;
	while (i < 8 && j < 8)
	{
		if (m_board[i][j] == 0) container.push_back(Container({ i, j, removes }));
		else
		if (m_board[i][j] == FIGURE_PLAYER2 || m_board[i][j] == SUPER_FIGURE_PLAYER2) break;
		else
			if (i + 1 < 8 && j + 1 < 8 && (m_board[i][j] == FIGURE_PLAYER1 || m_board[i][j] == SUPER_FIGURE_PLAYER1) && m_board[i + 1][j + 1] == 0)
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j))
				{
					removes.push_back(v.first);
					break;
				}
		}
		else break;
		i++; j++;
	}
	removes.clear();

	i = coor.first - 1; j = coor.second - 1;
	while (i >= 0 && j >= 0)
	{
		if (m_board[i][j] == 0) container.push_back(Container({ i, j, removes }));
		else
		if (m_board[i][j] == FIGURE_PLAYER2 || m_board[i][j] == SUPER_FIGURE_PLAYER2) break;
		else
			if (i - 1 >= 0 && j - 1 >= 0 && (m_board[i][j] == FIGURE_PLAYER1 || m_board[i][j] == SUPER_FIGURE_PLAYER1) && m_board[i - 1][j - 1] == 0)
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j))
				{
					removes.push_back(v.first);
					break;
				}
		}
		else break;

		i--; j--;
	}
	removes.clear();

	i = coor.first - 1; j = coor.second + 1;
	while (i >= 0 && j < 8)
	{
		if (m_board[i][j] == 0) container.push_back(Container({ i, j, removes }));
		else
		if (m_board[i][j] == FIGURE_PLAYER2 || m_board[i][j] == SUPER_FIGURE_PLAYER2) break;
		else
			if (i - 1 >= 0 && j + 1 < 8 && (m_board[i][j] == FIGURE_PLAYER1 || m_board[i][j] == SUPER_FIGURE_PLAYER1) && m_board[i - 1][j - 1] == 0)
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j))
				{
					removes.push_back(v.first);
					break;
				}
		}
		else break;
		i--; j++;
	}
	removes.clear();

	i = coor.first + 1; j = coor.second - 1;
	while (i < 8 && j >= 0)
	{
		if (m_board[i][j] == 0) container.push_back(Container({ i, j, removes }));
		else
		if (m_board[i][j] == FIGURE_PLAYER2 || m_board[i][j] == SUPER_FIGURE_PLAYER2) break;
		else
			if (i + 1 < 8 && j - 1 >= 0 && (m_board[i][j] == FIGURE_PLAYER1 || m_board[i][j] == SUPER_FIGURE_PLAYER1) && m_board[i + 1][j - 1] == 0)
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j))
				{
					removes.push_back(v.first);
					break;
				}
		}
		else break;
		i++; j--;
	}
	removes.clear();
}



void CheckersLogic::RecursivePlayer1(std::pair<int, int> coor, std::pair<int, int> prev,  std::vector<int>& removes, std::vector<Container>& container, int used[8][8])
{
	std::pair<int, int> sprev = coor;
	int i = coor.first - 1, j = coor.second;
	int tmp = m_board[sprev.first][sprev.second];

	if (i >= 0)
	{
		if (j + 1 < 8 && !m_board[i][j + 1] && m_board[i + 1][j] == FIGURE_PLAYER1) { container.push_back(Container({ i, j + 1, removes })); }
		if (j - 1 >= 0 && !m_board[i][j - 1] && m_board[i + 1][j] == FIGURE_PLAYER1) { container.push_back(Container({ i, j - 1, removes })); }
		if (removes.empty())
			m_board[sprev.first][sprev.second] = 0;
		if (j + 1 < 8 && (m_board[i][j + 1] == FIGURE_PLAYER2 || m_board[i][j + 1] == SUPER_FIGURE_PLAYER2) &&
			j + 2 < 8 && i - 1 >= 0 && !m_board[i - 1][j + 2] && !used[i - 1][j + 2])
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j + 1))
				{
					removes.push_back(v.first);
					break;
				}
			for (int q = 0; q < container.size(); q++)
				if (container[q].i == i - 1 && container[q].j == j + 2 && container[q].removes.size() < removes.size())
				{
					container.erase(container.begin() + q);
					break;
				}

			coor.first = i - 1;
			coor.second = j + 2;
			container.push_back(Container({ i - 1, j + 2, removes }));
			if (!used[i - 1][j + 2] && prev != std::make_pair(i - 1, j + 2))
			{
				used[coor.first][coor.second] = 1;
				RecursivePlayer1(coor, sprev, removes, container, used);
			}
			removes.pop_back();
		}

		if (j - 1 >= 0 && (m_board[i][j - 1] == FIGURE_PLAYER2 || m_board[i][j - 1] == SUPER_FIGURE_PLAYER2) &&
			j - 2 >= 0 && i - 1 >= 0 && !m_board[i - 1][j - 2] && !used[i - 1][j - 2])
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j - 1))
				{
					removes.push_back(v.first);
					break;
				}
			for (int q = 0; q < container.size(); q++)
				if (container[q].i == i - 1 && container[q].j == j - 2 && container[q].removes.size() < removes.size())
				{
					container.erase(container.begin() + q);
					break;
				}
			coor.first = i - 1;
			coor.second = j - 2;
			container.push_back(Container({ i - 1, j - 2, removes }));

			if (!used[i - 1][j - 2] && prev != std::make_pair(i - 1, j - 2))
			{
				used[coor.first][coor.second] = 1;
				RecursivePlayer1(coor, sprev, removes, container, used);
			}
			removes.pop_back();
		}
	}
	i += 2;
	if (i < 8)
	{
		if (j + 1 < 8 && (m_board[i][j + 1] == FIGURE_PLAYER2 || m_board[i][j + 1] == SUPER_FIGURE_PLAYER2) &&
			j + 2 < 8 && i + 1 >= 0 && !m_board[i + 1][j + 2])
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j + 1))
				{
					removes.push_back(v.first);
					break;
				}

			for (int q = 0; q < container.size(); q++)
				if (container[q].i == i + 1 && container[q].j == j + 2 && container[q].removes.size() <= removes.size())
				{
					container.erase(container.begin() + q);
					break;
				}
			coor.first = i + 1;
			coor.second = j + 2;
			container.push_back(Container({ i + 1, j + 2, removes }));
			if (!used[i + 1][j + 2] && prev != std::make_pair(i + 1, j + 2))
			{
				used[coor.first][coor.second] = 1;
				RecursivePlayer1(coor, sprev, removes, container, used);
			}
			removes.pop_back();
		}

		if (j - 1 >= 0 && (m_board[i][j - 1] == FIGURE_PLAYER2 || m_board[i][j - 1] == SUPER_FIGURE_PLAYER2) &&
			j - 2 >= 0 && i + 1 >= 0 && !m_board[i + 1][j - 2] && !used[i + 1][j - 2])
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j - 1))
				{
					removes.push_back(v.first);
					break;
				}

			for (int q = 0; q < container.size(); q++)
				if (container[q].i == i + 1 && container[q].j == j - 2 && container[q].removes.size() < removes.size())
				{
					container.erase(container.begin() + q);
					break;
				}

			coor.first = i + 1;
			coor.second = j - 2;
			container.push_back(Container({ i + 1, j - 2, removes }));
			if (!used[i + 1][j - 2] && prev != std::make_pair(i + 1, j - 2))
			{
				used[coor.first][coor.second] = 1;
				RecursivePlayer1(coor, sprev, removes, container, used);
			}

			removes.pop_back();
		}
	}
	if (removes.empty())
		m_board[sprev.first][sprev.second] = tmp;
}

void CheckersLogic::RecursivePlayer2(std::pair<int, int> coor, std::pair<int, int> prev, std::vector<int>& removes, std::vector<Container>& container, int used[8][8])
{
	std::pair<int, int> sprev = coor;
	int i = coor.first + 1, j = coor.second;
	int tmp = m_board[sprev.first][sprev.second];
	if (i < 8)
	{
		if (j + 1 < 8 && !m_board[i][j + 1] && m_board[i - 1][j] == FIGURE_PLAYER2) { container.push_back(Container({ i, j + 1, removes })); }
		if (j - 1 >= 0 && !m_board[i][j - 1] && m_board[i - 1][j] == FIGURE_PLAYER2) { container.push_back(Container({ i, j - 1, removes })); }
		if (removes.empty())
			m_board[sprev.first][sprev.second] = 0;
		if (j + 1 < 8 && (m_board[i][j + 1] == FIGURE_PLAYER1 || m_board[i][j + 1] == SUPER_FIGURE_PLAYER1) &&
			j + 2 < 8 && i + 1 >= 0 && !m_board[i + 1][j + 2])
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j + 1))
				{
					removes.push_back(v.first);
					break;
				}

			for (int q = 0; q < container.size(); q++)
				if (container[q].i == i + 1 && container[q].j == j + 2 && container[q].removes.size() <= removes.size())
				{
					container.erase(container.begin() + q);
					break;
				}
			coor.first = i + 1;
			coor.second = j + 2;
			container.push_back(Container({ i + 1, j + 2, removes }));
			if (!used[i + 1][j + 2] && prev != std::make_pair(i + 1, j + 2))
			{
				used[coor.first][coor.second] = 1;
				RecursivePlayer2(coor, sprev, removes, container, used);
			}
			removes.pop_back();
		}

		if (j - 1 >= 0 && (m_board[i][j - 1] == FIGURE_PLAYER1 || m_board[i][j - 1] == SUPER_FIGURE_PLAYER1) &&
			j - 2 >= 0 && i + 1 >= 0 && !m_board[i + 1][j - 2] && !used[i + 1][j - 2])
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j - 1))
				{
					removes.push_back(v.first);
					break;
				}

			for (int q = 0; q < container.size(); q++)
				if (container[q].i == i + 1 && container[q].j == j - 2 && container[q].removes.size() < removes.size())
				{
					container.erase(container.begin() + q);
					break;
				}

			coor.first = i + 1;
			coor.second = j - 2;
			container.push_back(Container({ i + 1, j - 2, removes }));
			if (!used[i + 1][j - 2] && prev != std::make_pair(i + 1, j - 2))
			{
				used[coor.first][coor.second] = 1;
				RecursivePlayer2(coor, sprev, removes, container, used);
			}

			removes.pop_back();
		}

	}

	i -= 2;
	if (i >= 0)
	{
		if (j + 1 < 8 && (m_board[i][j + 1] == FIGURE_PLAYER1 || m_board[i][j + 1] == SUPER_FIGURE_PLAYER1) &&
			j + 2 < 8 && i - 1 >= 0 && !m_board[i - 1][j + 2] && !used[i - 1][j + 2])
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j + 1))
				{
					removes.push_back(v.first);
					break;
				}
			for (int q = 0; q < container.size(); q++)
				if (container[q].i == i - 1 && container[q].j == j + 2 && container[q].removes.size() < removes.size())
				{
					container.erase(container.begin() + q);
					break;
				}

			coor.first = i - 1;
			coor.second = j + 2;
			container.push_back(Container({ i - 1, j + 2, removes }));
			if (!used[i - 1][j + 2] && prev != std::make_pair(i - 1, j + 2))
			{
				used[coor.first][coor.second] = 1;
				RecursivePlayer2(coor, sprev, removes, container, used);
			}
			removes.pop_back();
		}

		if (j - 1 >= 0 && (m_board[i][j - 1] == FIGURE_PLAYER1 || m_board[i][j - 1] == SUPER_FIGURE_PLAYER1) &&
			j - 2 >= 0 && i - 1 >= 0 && !m_board[i - 1][j - 2] && !used[i - 1][j - 2])
		{
			for (auto & v : m_figure_mapping)
				if (v.second == std::make_pair(i, j - 1))
				{
					removes.push_back(v.first);
					break;
				}
			for (int q = 0; q < container.size(); q++)
				if (container[q].i == i - 1 && container[q].j == j - 2 && container[q].removes.size() < removes.size())
				{
					container.erase(container.begin() + q);
					break;
				}
			coor.first = i - 1;
			coor.second = j - 2;
			container.push_back(Container({ i - 1, j - 2, removes }));

			if (!used[i - 1][j - 2] && prev != std::make_pair(i - 1, j - 2))
			{
				used[coor.first][coor.second] = 1;
				RecursivePlayer2(coor, sprev, removes, container, used);
			}
			removes.pop_back();
		}
	}

	if (removes.empty())
		m_board[sprev.first][sprev.second] = tmp;
}